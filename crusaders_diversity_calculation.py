#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#from __future__ import absolute_import, division, print_function, unicode_literals

import os
import sys



tagCount=0
crusaderCount=0
headerList=[]
crusadersList=[]
def loadInputFile(fileName):
    global crusadersList
    global crusaderCount, tagCount
    print ("Loading file: '" + fileName + "'")
    lines = [line.rstrip("\r\n") for line in open(fileName)]

    #-- Let's parse, everything is TAB separated
    # 1st line is the header with tag names, e.g. "Cursader        Male    Female  Human   Animal  Royal"
    # All other lines are crusaders, with the very first being Baenarall
    #   e.g. "The Bush Whacker<------>1<-----><------>1<-----><------>..."
    indexLine=0
    countBlock=0
    for currentLine in lines:
        lineList=currentLine.split("\t")
        lineList=[string.strip() for string in lineList]
        #print( "lineList[#{0}]={1}".format(len(lineList), str(lineList) ) )
        if indexLine == 0:
            headerList=lineList
        else:
            if currentLine[0:1] != "#":
                crusadersList.append(lineList)
                crusaderCount+=1
        indexLine+=1
    print("crusadersList:")
    print(crusadersList)
    print("---")
    tagCount=len(headerList)
    print( "Input file loaded: crusaderCount={0}, tagCount={1}".format(crusaderCount, tagCount ) )
#-- END loadInputFile()


loadInputFile("crusaders_input.txt")


#tagCount=20
#crusaderCount=6
#print("PARSING DONE")
#print("  crusaderCount={0}, tagCount={1}".format(crusaderCount, tagCount))

array=list(range (1,crusaderCount));
#print( "array is "+str(array) )

allResultsList=[]

combinationCounter=0
bonus=60
penalty=-15
def processCombination(inputArray):
    global combinationCounter
    global crusadersList
    result=0
    tryList=[0] + inputArray
    combinationCounter+=1
    
    #-- count the total number of each tag in the formation for the current crusaders combination
    tagCountList=[ 0 for x in range(tagCount) ]
    for i in tryList:
        #print("  # i={0}".format(i))
        #print( "adding crusader: "+str(crusadersList[i]) )
        #-- check all tags for crusader $crusadersList[i]
        j=0
        for tagString in crusadersList[i]:
            if tagString == "1":
                tagCountList[j]+=1
            elif tagString == "2":  #-- some crusaders have 2 of the same tag (e.g. Carmen and Petunia)
                tagCountList[j]+=2
            j+=1
    #print( "  # tagCountList="+str(tagCountList) )
    
    # WARNING: new formula 2019-10: if tagCount>=1 then +bonus, if tagCount>=2 then additionally +penalty
    # Old formula: if tagCount==1 then +bonus elif tagCount>1 then +penalty
    # TODO: new formula
    for i in tagCountList:
        if i>=1:
            result+=bonus
        if i>=2:
            result+=penalty

    #-- compile a simple list with result and crusader names
    resultItem=[result]
    for i in tryList:
        resultItem.append(crusadersList[i][0])
    print("combination {0}, resultItem={1}".format(combinationCounter, resultItem) )
    allResultsList.append(resultItem)


#-- sofar[] is empty at the beginning, rest[] is the full array at the beginning, n is the length of combination from
#     $rest - i.e. n-combination of set $rest[]
#
#   Here we use the principle that an n-combination of rest[] is rest[i] + (n-1)-combination of remaining after rest[i]
#     for each i.
#   e.g. (we ommit sofar[] here) comb([1,2,3],2) is [1] + comb([2,3],1) ; [2] + comb([3],1) ; [3] + comb([],1)
#   Note: comb([],1) will not lead to any result
#         [2]+comb([3],1) will spawn [2,3]+comb([],0) giving [2,3]
#         [1]+comb([2,3],1) will spawn [1,2]+comb([3],0) and [1,3]+comb([],0) giving [1,2] and [1,3]
def comb(sofar, rest, n):
    if n == 0:
        #print(str(sofar))
        processCombination(sofar)
    else:
        for i in range(len(rest)):
            #-- call comb with base sofar[]+rest[i], and n-1 combinations over rest[i+1:]
            comb(list(sofar + rest[i:i+1]), list(rest[i+1:]), n-1)


#-- serch for combinations of Baenarall with 2, 3 or 4 crusaders

#sys.exit(1)
comb([ ], array, 2)
comb([ ], array, 3)
comb([ ], array, 4)

#-- now print top 30 from allResultsList
i=0
for item in sorted(allResultsList, key=lambda x:x[0], reverse=True):
    print(str(item))
    i+=1
    if i>=30:
        break
